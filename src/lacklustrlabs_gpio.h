#pragma once

#include <Arduino.h>
#if defined(ARDUINO_ARCH_STM32F1)

#if defined(BOARD_maple_mini)
#include <lacklustrlabs_gpio_maple_mini.h>
#else
#error "No board definition found"
#endif

#if defined(ARDUINO) && ARDUINO >=100
#include <Arduino.h>
#else
#include <WProgram.h>
#endif

namespace lacklustrlabs {
namespace gpio {

template<uint8_t _pin>
class GPIO {

  public:
  GPIO() {
  }

  //const gpio_reg_map * const _regmap = lacklustrlabs::getGpioRegMap(_pin);
  volatile uint32_t* const _set = ::lacklustrlabs::gpio::getGpioSetRegister(_pin);
  volatile const uint32_t* const _input = ::lacklustrlabs::gpio::getGpioInput(_pin);
  volatile uint32_t* const _clear = ::lacklustrlabs::gpio::getGpioClearRegister(_pin);
  const uint16_t _pinMask = ::lacklustrlabs::gpio::getGpioBitMask(_pin);


  inline void digitalWrite(bool state) {
    if(state) {
      *_set |= _pinMask;
    } else {
      *_clear |= _pinMask;
    }
  }

  inline bool digitalRead() {
    return *_input & _pinMask;
  }

  inline void set() {
    *_set |= _pinMask;
  }

  inline void clear() {
    *_clear |= _pinMask;
  }

  /*
   *  verify that a GPIO<pin> has all the correct values
   */
  bool sanityCheck() {
    return _set == portSetRegister(_pin) &&
        _clear == portClearRegister(_pin) &&
        _input == portInputRegister(digitalPinToPort(_pin)) &&
        _pinMask == digitalPinToBitMask(_pin);
  }
};

/**
 * An alternative implementation of GPIO that supports inverted logic.
 * The compiled code of GPIOe<pin,false> should be byte-identical to GPIO<pin>. TODO: check that  
 */
template<uint8_t _pin, bool _invertedLogic=false>
class GPIOe : public GPIO<_pin> {
public:
  GPIOe() : GPIO<_pin>() {
  }

  inline void digitalWrite(bool state) {
    GPIO<_pin>::digitalWrite(_invertedLogic?!state:state);
  }

  inline bool digitalRead() {
    return _invertedLogic?!GPIO<_pin>::digitalRead():GPIO<_pin>::digitalRead();
  }

  inline void set() {
    if(_invertedLogic)
      GPIO<_pin>::clear();
    else
      GPIO<_pin>::set();
  }

  inline void clear() {
    if(_invertedLogic)
      GPIO<_pin>::set();
    else
      GPIO<_pin>::clear();
  }
};

} // namespace gpio
} // namespace lacklustrlabs

#endif // ARDUINO_ARCH_STM32F1

