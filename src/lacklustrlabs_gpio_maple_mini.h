#pragma once

#if defined(ARDUINO_ARCH_STM32F1) && defined(BOARD_maple_mini)

namespace lacklustrlabs {
namespace gpio {

static constexpr const gpio_reg_map* getGpioRegMap(uint8_t pin);
static constexpr const gpio_reg_map* getGpioRegMap(uint8_t pin) {
  return pin==0?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
          pin==1?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
           pin==2?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
            pin==3?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
             pin==4?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
              pin==5?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
               pin==6?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                pin==7?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                 pin==8?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                  pin==9?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                   pin==10?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                    pin==11?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                     pin==12?reinterpret_cast<const gpio_reg_map*>(GPIOC_BASE):
                      pin==13?reinterpret_cast<const gpio_reg_map*>(GPIOC_BASE):
                       pin==14?reinterpret_cast<const gpio_reg_map*>(GPIOC_BASE):
                        pin==15?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
                         pin==16?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
                          pin==17?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
                           pin==18?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
                            pin==19?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
                             pin==20?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                              pin==21?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                               pin==22?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                                pin==23?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                                 pin==24?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                                  pin==25?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                                   pin==26?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                                    pin==27?reinterpret_cast<const gpio_reg_map*>(GPIOA_BASE):
                                     pin==28?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
                                      pin==29?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
                                       pin==30?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
                                        pin==31?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
                                         pin==32?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
                                          pin==33?reinterpret_cast<const gpio_reg_map*>(GPIOB_BASE):
                                           nullptr; // this should never happen
}


static constexpr volatile uint32_t* getGpioOutput(uint8_t pin);
static constexpr volatile uint32_t* getGpioOutput(uint8_t pin) {
  return pin==0?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
          pin==1?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
           pin==2?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
            pin==3?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
             pin==4?reinterpret_cast<volatile uint32_t*>(0x4001080C):
              pin==5?reinterpret_cast<volatile uint32_t*>(0x4001080C):
               pin==6?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                pin==7?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                 pin==8?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                  pin==9?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                   pin==10?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                    pin==11?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                     pin==12?reinterpret_cast<volatile uint32_t*>(0x4001100C):
                      pin==13?reinterpret_cast<volatile uint32_t*>(0x4001100C):
                       pin==14?reinterpret_cast<volatile uint32_t*>(0x4001100C):
                        pin==15?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
                         pin==16?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
                          pin==17?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
                           pin==18?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
                            pin==19?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
                             pin==20?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                              pin==21?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                               pin==22?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                                pin==23?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                                 pin==24?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                                  pin==25?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                                   pin==26?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                                    pin==27?reinterpret_cast<volatile uint32_t*>(0x4001080C):
                                     pin==28?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
                                      pin==29?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
                                       pin==30?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
                                        pin==31?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
                                         pin==32?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
                                          pin==33?reinterpret_cast<volatile uint32_t*>(0x40010C0C):
                                           nullptr; // this should never happen
}


static constexpr const volatile uint32_t* getGpioInput(uint8_t pin);
static constexpr const volatile uint32_t* getGpioInput(uint8_t pin) {
  return pin==0?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
          pin==1?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
           pin==2?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
            pin==3?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
             pin==4?reinterpret_cast<const volatile uint32_t*>(0x40010808):
              pin==5?reinterpret_cast<const volatile uint32_t*>(0x40010808):
               pin==6?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                pin==7?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                 pin==8?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                  pin==9?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                   pin==10?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                    pin==11?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                     pin==12?reinterpret_cast<const volatile uint32_t*>(0x40011008):
                      pin==13?reinterpret_cast<const volatile uint32_t*>(0x40011008):
                       pin==14?reinterpret_cast<const volatile uint32_t*>(0x40011008):
                        pin==15?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
                         pin==16?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
                          pin==17?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
                           pin==18?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
                            pin==19?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
                             pin==20?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                              pin==21?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                               pin==22?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                                pin==23?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                                 pin==24?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                                  pin==25?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                                   pin==26?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                                    pin==27?reinterpret_cast<const volatile uint32_t*>(0x40010808):
                                     pin==28?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
                                      pin==29?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
                                       pin==30?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
                                        pin==31?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
                                         pin==32?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
                                          pin==33?reinterpret_cast<const volatile uint32_t*>(0x40010C08):
                                           nullptr; // this should never happen
}


static constexpr uint16_t getGpioBitMask(uint8_t pin);
static constexpr uint16_t getGpioBitMask(uint8_t pin) {
  return pin==0?(uint16_t)0x800:
          pin==1?(uint16_t)0x400:
           pin==2?(uint16_t)0x4:
            pin==3?(uint16_t)0x1:
             pin==4?(uint16_t)0x80:
              pin==5?(uint16_t)0x40:
               pin==6?(uint16_t)0x20:
                pin==7?(uint16_t)0x10:
                 pin==8?(uint16_t)0x8:
                  pin==9?(uint16_t)0x4:
                   pin==10?(uint16_t)0x2:
                    pin==11?(uint16_t)0x1:
                     pin==12?(uint16_t)0x8000:
                      pin==13?(uint16_t)0x4000:
                       pin==14?(uint16_t)0x2000:
                        pin==15?(uint16_t)0x80:
                         pin==16?(uint16_t)0x40:
                          pin==17?(uint16_t)0x20:
                           pin==18?(uint16_t)0x10:
                            pin==19?(uint16_t)0x8:
                             pin==20?(uint16_t)0x8000:
                              pin==21?(uint16_t)0x4000:
                               pin==22?(uint16_t)0x2000:
                                pin==23?(uint16_t)0x1000:
                                 pin==24?(uint16_t)0x800:
                                  pin==25?(uint16_t)0x400:
                                   pin==26?(uint16_t)0x200:
                                    pin==27?(uint16_t)0x100:
                                     pin==28?(uint16_t)0x8000:
                                      pin==29?(uint16_t)0x4000:
                                       pin==30?(uint16_t)0x2000:
                                        pin==31?(uint16_t)0x1000:
                                         pin==32?(uint16_t)0x100:
                                          pin==33?(uint16_t)0x2:
                                           (uint16_t)0x0000; // this should never happen
}


static constexpr volatile uint32_t* getGpioSetRegister(uint8_t pin);
static constexpr volatile uint32_t* getGpioSetRegister(uint8_t pin) {
  return pin==0?reinterpret_cast<volatile uint32_t*>(0x40010C10):
          pin==1?reinterpret_cast<volatile uint32_t*>(0x40010C10):
           pin==2?reinterpret_cast<volatile uint32_t*>(0x40010C10):
            pin==3?reinterpret_cast<volatile uint32_t*>(0x40010C10):
             pin==4?reinterpret_cast<volatile uint32_t*>(0x40010810):
              pin==5?reinterpret_cast<volatile uint32_t*>(0x40010810):
               pin==6?reinterpret_cast<volatile uint32_t*>(0x40010810):
                pin==7?reinterpret_cast<volatile uint32_t*>(0x40010810):
                 pin==8?reinterpret_cast<volatile uint32_t*>(0x40010810):
                  pin==9?reinterpret_cast<volatile uint32_t*>(0x40010810):
                   pin==10?reinterpret_cast<volatile uint32_t*>(0x40010810):
                    pin==11?reinterpret_cast<volatile uint32_t*>(0x40010810):
                     pin==12?reinterpret_cast<volatile uint32_t*>(0x40011010):
                      pin==13?reinterpret_cast<volatile uint32_t*>(0x40011010):
                       pin==14?reinterpret_cast<volatile uint32_t*>(0x40011010):
                        pin==15?reinterpret_cast<volatile uint32_t*>(0x40010C10):
                         pin==16?reinterpret_cast<volatile uint32_t*>(0x40010C10):
                          pin==17?reinterpret_cast<volatile uint32_t*>(0x40010C10):
                           pin==18?reinterpret_cast<volatile uint32_t*>(0x40010C10):
                            pin==19?reinterpret_cast<volatile uint32_t*>(0x40010C10):
                             pin==20?reinterpret_cast<volatile uint32_t*>(0x40010810):
                              pin==21?reinterpret_cast<volatile uint32_t*>(0x40010810):
                               pin==22?reinterpret_cast<volatile uint32_t*>(0x40010810):
                                pin==23?reinterpret_cast<volatile uint32_t*>(0x40010810):
                                 pin==24?reinterpret_cast<volatile uint32_t*>(0x40010810):
                                  pin==25?reinterpret_cast<volatile uint32_t*>(0x40010810):
                                   pin==26?reinterpret_cast<volatile uint32_t*>(0x40010810):
                                    pin==27?reinterpret_cast<volatile uint32_t*>(0x40010810):
                                     pin==28?reinterpret_cast<volatile uint32_t*>(0x40010C10):
                                      pin==29?reinterpret_cast<volatile uint32_t*>(0x40010C10):
                                       pin==30?reinterpret_cast<volatile uint32_t*>(0x40010C10):
                                        pin==31?reinterpret_cast<volatile uint32_t*>(0x40010C10):
                                         pin==32?reinterpret_cast<volatile uint32_t*>(0x40010C10):
                                          pin==33?reinterpret_cast<volatile uint32_t*>(0x40010C10):
                                           nullptr; // this should never happen
}


static constexpr volatile uint32_t* getGpioClearRegister(uint8_t pin);
static constexpr volatile uint32_t* getGpioClearRegister(uint8_t pin) {
  return pin==0?reinterpret_cast<volatile uint32_t*>(0x40010C14):
          pin==1?reinterpret_cast<volatile uint32_t*>(0x40010C14):
           pin==2?reinterpret_cast<volatile uint32_t*>(0x40010C14):
            pin==3?reinterpret_cast<volatile uint32_t*>(0x40010C14):
             pin==4?reinterpret_cast<volatile uint32_t*>(0x40010814):
              pin==5?reinterpret_cast<volatile uint32_t*>(0x40010814):
               pin==6?reinterpret_cast<volatile uint32_t*>(0x40010814):
                pin==7?reinterpret_cast<volatile uint32_t*>(0x40010814):
                 pin==8?reinterpret_cast<volatile uint32_t*>(0x40010814):
                  pin==9?reinterpret_cast<volatile uint32_t*>(0x40010814):
                   pin==10?reinterpret_cast<volatile uint32_t*>(0x40010814):
                    pin==11?reinterpret_cast<volatile uint32_t*>(0x40010814):
                     pin==12?reinterpret_cast<volatile uint32_t*>(0x40011014):
                      pin==13?reinterpret_cast<volatile uint32_t*>(0x40011014):
                       pin==14?reinterpret_cast<volatile uint32_t*>(0x40011014):
                        pin==15?reinterpret_cast<volatile uint32_t*>(0x40010C14):
                         pin==16?reinterpret_cast<volatile uint32_t*>(0x40010C14):
                          pin==17?reinterpret_cast<volatile uint32_t*>(0x40010C14):
                           pin==18?reinterpret_cast<volatile uint32_t*>(0x40010C14):
                            pin==19?reinterpret_cast<volatile uint32_t*>(0x40010C14):
                             pin==20?reinterpret_cast<volatile uint32_t*>(0x40010814):
                              pin==21?reinterpret_cast<volatile uint32_t*>(0x40010814):
                               pin==22?reinterpret_cast<volatile uint32_t*>(0x40010814):
                                pin==23?reinterpret_cast<volatile uint32_t*>(0x40010814):
                                 pin==24?reinterpret_cast<volatile uint32_t*>(0x40010814):
                                  pin==25?reinterpret_cast<volatile uint32_t*>(0x40010814):
                                   pin==26?reinterpret_cast<volatile uint32_t*>(0x40010814):
                                    pin==27?reinterpret_cast<volatile uint32_t*>(0x40010814):
                                     pin==28?reinterpret_cast<volatile uint32_t*>(0x40010C14):
                                      pin==29?reinterpret_cast<volatile uint32_t*>(0x40010C14):
                                       pin==30?reinterpret_cast<volatile uint32_t*>(0x40010C14):
                                        pin==31?reinterpret_cast<volatile uint32_t*>(0x40010C14):
                                         pin==32?reinterpret_cast<volatile uint32_t*>(0x40010C14):
                                          pin==33?reinterpret_cast<volatile uint32_t*>(0x40010C14):
                                           nullptr; // this should never happen
}


static constexpr volatile uint32_t* getGpioConfigRegister(uint8_t pin);
static constexpr volatile uint32_t* getGpioConfigRegister(uint8_t pin) {
  return pin==0?reinterpret_cast<volatile uint32_t*>(0x40010C00):
          pin==1?reinterpret_cast<volatile uint32_t*>(0x40010C00):
           pin==2?reinterpret_cast<volatile uint32_t*>(0x40010C00):
            pin==3?reinterpret_cast<volatile uint32_t*>(0x40010C00):
             pin==4?reinterpret_cast<volatile uint32_t*>(0x40010800):
              pin==5?reinterpret_cast<volatile uint32_t*>(0x40010800):
               pin==6?reinterpret_cast<volatile uint32_t*>(0x40010800):
                pin==7?reinterpret_cast<volatile uint32_t*>(0x40010800):
                 pin==8?reinterpret_cast<volatile uint32_t*>(0x40010800):
                  pin==9?reinterpret_cast<volatile uint32_t*>(0x40010800):
                   pin==10?reinterpret_cast<volatile uint32_t*>(0x40010800):
                    pin==11?reinterpret_cast<volatile uint32_t*>(0x40010800):
                     pin==12?reinterpret_cast<volatile uint32_t*>(0x40011000):
                      pin==13?reinterpret_cast<volatile uint32_t*>(0x40011000):
                       pin==14?reinterpret_cast<volatile uint32_t*>(0x40011000):
                        pin==15?reinterpret_cast<volatile uint32_t*>(0x40010C00):
                         pin==16?reinterpret_cast<volatile uint32_t*>(0x40010C00):
                          pin==17?reinterpret_cast<volatile uint32_t*>(0x40010C00):
                           pin==18?reinterpret_cast<volatile uint32_t*>(0x40010C00):
                            pin==19?reinterpret_cast<volatile uint32_t*>(0x40010C00):
                             pin==20?reinterpret_cast<volatile uint32_t*>(0x40010800):
                              pin==21?reinterpret_cast<volatile uint32_t*>(0x40010800):
                               pin==22?reinterpret_cast<volatile uint32_t*>(0x40010800):
                                pin==23?reinterpret_cast<volatile uint32_t*>(0x40010800):
                                 pin==24?reinterpret_cast<volatile uint32_t*>(0x40010800):
                                  pin==25?reinterpret_cast<volatile uint32_t*>(0x40010800):
                                   pin==26?reinterpret_cast<volatile uint32_t*>(0x40010800):
                                    pin==27?reinterpret_cast<volatile uint32_t*>(0x40010800):
                                     pin==28?reinterpret_cast<volatile uint32_t*>(0x40010C00):
                                      pin==29?reinterpret_cast<volatile uint32_t*>(0x40010C00):
                                       pin==30?reinterpret_cast<volatile uint32_t*>(0x40010C00):
                                        pin==31?reinterpret_cast<volatile uint32_t*>(0x40010C00):
                                         pin==32?reinterpret_cast<volatile uint32_t*>(0x40010C00):
                                          pin==33?reinterpret_cast<volatile uint32_t*>(0x40010C00):
                                           nullptr; // this should never happen
}


static constexpr int16_t getGpioBit(uint8_t pin);
static constexpr int16_t getGpioBit(uint8_t pin) {
  return pin==0?(int16_t)11:
          pin==1?(int16_t)10:
           pin==2?(int16_t)2:
            pin==3?(int16_t)0:
             pin==4?(int16_t)7:
              pin==5?(int16_t)6:
               pin==6?(int16_t)5:
                pin==7?(int16_t)4:
                 pin==8?(int16_t)3:
                  pin==9?(int16_t)2:
                   pin==10?(int16_t)1:
                    pin==11?(int16_t)0:
                     pin==12?(int16_t)15:
                      pin==13?(int16_t)14:
                       pin==14?(int16_t)13:
                        pin==15?(int16_t)7:
                         pin==16?(int16_t)6:
                          pin==17?(int16_t)5:
                           pin==18?(int16_t)4:
                            pin==19?(int16_t)3:
                             pin==20?(int16_t)15:
                              pin==21?(int16_t)14:
                               pin==22?(int16_t)13:
                                pin==23?(int16_t)12:
                                 pin==24?(int16_t)11:
                                  pin==25?(int16_t)10:
                                   pin==26?(int16_t)9:
                                    pin==27?(int16_t)8:
                                     pin==28?(int16_t)15:
                                      pin==29?(int16_t)14:
                                       pin==30?(int16_t)13:
                                        pin==31?(int16_t)12:
                                         pin==32?(int16_t)8:
                                          pin==33?(int16_t)1:
                                           (int16_t)-1; // this should never happen
}


static constexpr exti_num getGpioExti(uint8_t pin);
static constexpr exti_num getGpioExti(uint8_t pin) {
  return pin==0?EXTI11:
          pin==1?EXTI10:
           pin==2?EXTI2:
            pin==3?EXTI0:
             pin==4?EXTI7:
              pin==5?EXTI6:
               pin==6?EXTI5:
                pin==7?EXTI4:
                 pin==8?EXTI3:
                  pin==9?EXTI2:
                   pin==10?EXTI1:
                    pin==11?EXTI0:
                     pin==12?EXTI15:
                      pin==13?EXTI14:
                       pin==14?EXTI13:
                        pin==15?EXTI7:
                         pin==16?EXTI6:
                          pin==17?EXTI5:
                           pin==18?EXTI4:
                            pin==19?EXTI3:
                             pin==20?EXTI15:
                              pin==21?EXTI14:
                               pin==22?EXTI13:
                                pin==23?EXTI12:
                                 pin==24?EXTI11:
                                  pin==25?EXTI10:
                                   pin==26?EXTI9:
                                    pin==27?EXTI8:
                                     pin==28?EXTI15:
                                      pin==29?EXTI14:
                                       pin==30?EXTI13:
                                        pin==31?EXTI12:
                                         pin==32?EXTI8:
                                          pin==33?EXTI1:
                                           EXTI15; // this should never happen
}

} // gpio
} // lacklustrlabs

#else // defined(ARDUINO_ARCH_STM32F1) && defined(BOARD_maple_mini)
#error "You should not include the wrong board definition file"
#endif

