#include <lacklustrlabs_gpio.h>

#if defined(ARDUINO_ARCH_STM32F1)
#define Serial Serial3
constexpr uint8_t LED_PIN = 33;
#else
constexpr uint8_t LED_PIN = 13;
#endif

using namespace lacklustrlabs::gpio;

GPIO<LED_PIN> pin;
GPIOe<LED_PIN, true> pinInverted;

void setup() {
  pinMode(LED_PIN, OUTPUT);
  Serial.begin(38400); //115200);
  while (!Serial) 
    ;
}

void loop() { 
  pin.digitalWrite(true);
  delay(500);
  pin.digitalWrite(false);
  delay(1000);
  
  pinInverted.digitalWrite(false);
  delay(500);
  pinInverted.digitalWrite(true);
  delay(1000); 
}
