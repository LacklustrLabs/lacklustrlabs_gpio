
//#define Serial Serial3

static constexpr uint8_t MAX_PIN = 33;
static constexpr uint8_t MIN_PIN = 0;

String toRegMapString(uint16_t pin) {
  const gpio_reg_map* const rm = digitalPinToPort(pin)->regs;

  if (rm==GPIOA_BASE) 
    return "GPIOA_BASE";
  else if (rm==GPIOB_BASE) 
    return "GPIOB_BASE";
  else if (rm==GPIOC_BASE) 
    return "GPIOC_BASE";
  return "unknown"; 
}

void buildGPIO_BASE() {
  Serial.println();
  Serial.print("static constexpr const gpio_reg_map* getGpioRegMap(uint8_t pin);"); 
  Serial.println();
  Serial.print("static constexpr const gpio_reg_map* getGpioRegMap(uint8_t pin) {");
  Serial.println();
  Serial.print("  return ");

  String indent = "          ";
  for (uint8_t pin=MIN_PIN; pin<=MAX_PIN; pin++) {
    Serial.print("pin==");
    Serial.print(pin);
    Serial.print("?reinterpret_cast<const gpio_reg_map*>(");
    Serial.print(toRegMapString(pin));
    Serial.print("):");
    Serial.println();
    Serial.print(indent);
    indent += " ";
  }
  Serial.println("nullptr; // this should never happen");
  Serial.println("}");
}

void buildGPIO_BitMask() {
  Serial.println();
  Serial.print("static constexpr uint16_t getGpioBitMask(uint8_t pin);"); 
  Serial.println();
  Serial.print("static constexpr uint16_t getGpioBitMask(uint8_t pin) {");
  Serial.println();
  Serial.print("  return ");

  String indent = "          ";
  for (uint8_t pin=MIN_PIN; pin<=MAX_PIN; pin++) {
    Serial.print("pin==");
    Serial.print(pin);
    Serial.print("?(uint16_t)0x");
    Serial.print((uint16_t)digitalPinToBitMask(pin), HEX);
    Serial.print(":");
    Serial.println();
    Serial.print(indent);
    indent += " ";
  }
  Serial.println("(uint16_t)0x0000; // this should never happen");
  Serial.println("}");
}

void buildGPIO_input() {
  Serial.println();
  Serial.print("static constexpr const volatile uint32_t* getGpioInput(uint8_t pin);"); 
  Serial.println();
  Serial.print("static constexpr const volatile uint32_t* getGpioInput(uint8_t pin) {");
  Serial.println();
  Serial.print("  return ");

  String indent = "          ";
  for (uint8_t pin=MIN_PIN; pin<=MAX_PIN; pin++) {
    Serial.print("pin==");
    Serial.print(pin);
    Serial.print("?reinterpret_cast<const volatile uint32_t*>(0x");
    Serial.print((uint32_t)portInputRegister(digitalPinToPort(pin)), HEX);
    Serial.print("):");
    Serial.println();
    Serial.print(indent);
    indent += " ";
  }
  Serial.println("nullptr; // this should never happen");
  Serial.println("}");
}

void buildGPIO_output() {
  Serial.println();
  Serial.print("static constexpr volatile uint32_t* getGpioOutput(uint8_t pin);"); 
  Serial.println();
  Serial.print("static constexpr volatile uint32_t* getGpioOutput(uint8_t pin) {");
  Serial.println();
  Serial.print("  return ");

  String indent = "          ";
  for (uint8_t pin=MIN_PIN; pin<=MAX_PIN; pin++) {
    Serial.print("pin==");
    Serial.print(pin);
    Serial.print("?reinterpret_cast<volatile uint32_t*>(0x");
    Serial.print((uint32_t)portOutputRegister(digitalPinToPort(pin)), HEX);
    Serial.print("):");
    Serial.println();
    Serial.print(indent);
    indent += " ";
  }
  Serial.println("nullptr; // this should never happen");
  Serial.println("}");
}

void buildGPIO_setRegister() {
  Serial.println();
  Serial.print("static constexpr volatile uint32_t* getGpioSetRegister(uint8_t pin);"); 
  Serial.println();
  Serial.print("static constexpr volatile uint32_t* getGpioSetRegister(uint8_t pin) {");
  Serial.println();
  Serial.print("  return ");

  String indent = "          ";
  for (uint8_t pin=MIN_PIN; pin<=MAX_PIN; pin++) {
    Serial.print("pin==");
    Serial.print(pin);
    Serial.print("?reinterpret_cast<volatile uint32_t*>(0x");
    Serial.print((uint32_t)portSetRegister(pin), HEX);
    Serial.print("):");
    Serial.println();
    Serial.print(indent);
    indent += " ";
  }
  Serial.println("nullptr; // this should never happen");
  Serial.println("}");
}

void buildGPIO_clearRegister() {
  Serial.println();
  Serial.print("static constexpr volatile uint32_t* getGpioClearRegister(uint8_t pin);"); 
  Serial.println();
  Serial.print("static constexpr volatile uint32_t* getGpioClearRegister(uint8_t pin) {");
  Serial.println();
  Serial.print("  return ");

  String indent = "          ";
  for (uint8_t pin=MIN_PIN; pin<=MAX_PIN; pin++) {
    Serial.print("pin==");
    Serial.print(pin);
    Serial.print("?reinterpret_cast<volatile uint32_t*>(0x");
    Serial.print((uint32_t)portClearRegister(pin), HEX);
    Serial.print("):");
    Serial.println();
    Serial.print(indent);
    indent += " ";
  }
  Serial.println("nullptr; // this should never happen");
  Serial.println("}");
}

void buildGPIO_configRegister() {
  Serial.println();
  Serial.print("static constexpr volatile uint32_t* getGpioConfigRegister(uint8_t pin);"); 
  Serial.println();
  Serial.print("static constexpr volatile uint32_t* getGpioConfigRegister(uint8_t pin) {");
  Serial.println();
  Serial.print("  return ");

  String indent = "          ";
  for (uint8_t pin=MIN_PIN; pin<=MAX_PIN; pin++) {
    Serial.print("pin==");
    Serial.print(pin);
    Serial.print("?reinterpret_cast<volatile uint32_t*>(0x");
    Serial.print((uint32_t)portConfigRegister(pin), HEX);
    Serial.print("):");
    Serial.println();
    Serial.print(indent);
    indent += " ";
  }
  Serial.println("nullptr; // this should never happen");
  Serial.println("}");
}


void buildGPIO_bit() {
  Serial.println();
  Serial.print("static constexpr int16_t getGpioBit(uint8_t pin);"); 
  Serial.println();
  Serial.print("static constexpr int16_t getGpioBit(uint8_t pin) {");
  Serial.println();
  Serial.print("  return ");

  String indent = "          ";
  for (uint8_t pin=MIN_PIN; pin<=MAX_PIN; pin++) {
    Serial.print("pin==");
    Serial.print(pin);
    Serial.print("?(int16_t)");
    Serial.print((PIN_MAP[pin].gpio_bit));
    Serial.print(":");
    Serial.println();
    Serial.print(indent);
    indent += " ";
  }
  Serial.println("(int16_t)-1; // this should never happen");
  Serial.println("}");
}

void buildGPIO_exti() {
  Serial.println();
  Serial.print("static constexpr exti_num getGpioExti(uint8_t pin);"); 
  Serial.println();
  Serial.print("static constexpr exti_num getGpioExti(uint8_t pin) {");
  Serial.println();
  Serial.print("  return ");

  String indent = "          ";
  for (uint8_t pin=MIN_PIN; pin<=MAX_PIN; pin++) {
    Serial.print("pin==");
    Serial.print(pin);
    Serial.print("?EXTI");
    Serial.print((PIN_MAP[pin].gpio_bit));
    Serial.print(":");
    Serial.println();
    Serial.print(indent);
    indent += " ";
  }
  Serial.println("EXTI15; // this should never happen");
  Serial.println("}");
}

void setup() {
  Serial.begin(115200);
  while (!Serial) 
    ;
}

void loop() {
  delay(5000);
  
  Serial.println();
  buildGPIO_BASE();

  Serial.println();
  buildGPIO_output();
  
  Serial.println();
  buildGPIO_input();
  
  Serial.println();
  buildGPIO_BitMask();
  
  Serial.println();
  buildGPIO_setRegister();

  Serial.println();
  buildGPIO_clearRegister();

  Serial.println();
  buildGPIO_configRegister();
  
  Serial.println();
  buildGPIO_bit();

  Serial.println();
  buildGPIO_exti();

  Serial.println();
}
